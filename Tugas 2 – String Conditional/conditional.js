//Soal 1 (if-else)

// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"


function gamewerewolf(nama,peran) {
	if (nama == '') {
		console.log('Nama harus diisi!')
	} else if (nama && peran == '') {
		console.log('Halo '+ nama +', Pilih peranmu untuk memulai game!')
	} else if (nama && peran == 'Penyihir') {
		console.log('Selamat datang di Dunia Werewolf, '+ nama +' \nHalo Penyihir '+ nama +', kamu dapat melihat siapa yang menjadi werewolf!')
	} else if (nama && peran == 'Guard') {
		console.log('Selamat datang di Dunia Werewolf, '+ nama +' \nHalo Guard '+ nama +', kamu akan membantu melindungi temanmu dari serangan werewolf.')
	} else if (nama && peran == 'Werewolf') {
		console.log('Selamat datang di Dunia Werewolf, '+ nama +' \nHalo Werewolf '+ nama +', Kamu akan memakan mangsa setiap malam!')
	}
}

gamewerewolf('','')
console.log('==============================')
gamewerewolf('John','')
console.log('==============================')
gamewerewolf('Jane','Penyihir')
console.log('==============================')
gamewerewolf('Jenita','Guard')
console.log('==============================')
gamewerewolf('Junaedi','Werewolf')
console.log('==============================')


//Soal 2 (switch case)
var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

switch(bulan) {
  case 1:   { console.log(NamaBulan = 'Januari'); break; }
  case 2:   { console.log(NamaBulan = 'Februari'); break; }
  case 3:   { console.log(NamaBulan = 'Maret'); break; }
  case 4:   { console.log(NamaBulan = 'April'); break; }
  case 5:   { console.log(NamaBulan = 'Mei'); break; }
  case 6:   { console.log(NamaBulan = 'Juni'); break; }
  case 7:   { console.log(NamaBulan = 'Juli'); break; }
  case 8:   { console.log(NamaBulan = 'Agustus'); break; }
  case 9:   { console.log(NamaBulan = 'September'); break; }
  case 10:  { console.log(NamaBulan = 'Oktober'); break; }
  case 11:  { console.log(NamaBulan = 'November'); break; }
  case 12:  { console.log(NamaBulan = 'Desember'); break; }
  default:  { console.log("Salah Nama Bulan"); break; }
}

console.log(tanggal + ' ' + NamaBulan + ' ' +tahun)
