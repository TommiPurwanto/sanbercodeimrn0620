//No. 1 Looping While
console.log('Looping While');
console.log('LOOPING PERTAMA');
var maju = 2;
while(maju < 21) {
  console.log(maju + ' - I love coding');
  maju+=2;
}
console.log('LOOPING KEDUA');
var mundur = 20;
while(mundur > 0) {
  console.log(mundur + '- I will become a mobile developer');
  mundur-=2;
}


//No. 2 Looping menggunakan for
console.log('\nLooping For');
var angka = 1;
for (i=angka; i<=20 ; i++){
  if ((i%2) == 0) {
    console.log( i + ' - Berkualitas');
  } else if ((i%3) == 0 && (i%2) == 1) {
    console.log( i + ' - I Love Coding');
  } else {
    console.log(i + ' - Santai');
  }
}


//No. 3 Membuat Persegi Panjang
console.log('\nMembuat Persegi Panjang');
var persegi='';

for (i=0; i<4; i++) {
  for (j=0; j<8; j++) {
    persegi+='#';
  }
  persegi+='\n';
}
console.log(persegi);


//No. 4 Membuat Tangga
console.log('\nMembuat Tangga');
var tangga='';

for (i=0; i<7; i++) {
  for (j=0; j<=i; j++) {
    tangga+='#';
  }
  tangga+='\n';
}
console.log(tangga);


//No. 5 Membuat Papan Catur
console.log('\nMembuat Papan Catur');
var catur = '';
for (i=0; i<8; i++) {
  for (j=0; j<8; j++) {
    if((j%2 == 0 && i%2 == 0) || (j%2 == 1 && i%2 == 1)) {
      catur+=' '
    } else {
      catur+='#'
    }
  }
  catur+='\n';
}
console.log(catur);