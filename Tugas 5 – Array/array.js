//Soal No. 1 (Range)
console.log('Soal No. 1 (Range)')
function range(startNum, finishNum) {
  var RangeArray=[]
  if(startNum < finishNum) {
    for(var i = startNum; i <= finishNum; i++) {
      RangeArray.push(i)
    } return RangeArray
  } else if(startNum > finishNum){
    for(var j = startNum; j >= finishNum; j--) {
      RangeArray.push(j)
    } return RangeArray
  } else if(startNum == undefined || finishNum == undefined) {
    RangeArray.push(-1)
  } return RangeArray
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


//Soal No. 2 (Range with Step)
console.log('\nSoal No. 2 (Range with Step)')
function rangeWithStep(startNum, finishNum, step) {
  var RangeStepArray=[]
  if(startNum < finishNum) {
    for(var i = startNum; i <= finishNum; i+= step) {
      RangeStepArray.push(i)
    } return RangeStepArray
  } else if(startNum > finishNum){
    for(var j = startNum; j >= finishNum; j-= step) {
      RangeStepArray.push(j)
    } return RangeStepArray
  }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


//Soal No. 3 (Sum of Range)
console.log('\nSoal No. 3 (Sum of Range)')
function sum(startNum, finishNum, step) {
  var total=0
  var array
  if(!startNum && !finishNum && !step) {
    total=0
  } else if (startNum && !finishNum && !step) {
    total=startNum
  } else {
    if (!step) {
      array = rangeWithStep(startNum,finishNum, 1)
    } else {
      array = rangeWithStep(startNum,finishNum, step)
    }
    for (i=0; i <array.length; i++) {
      total = total + array[i]
    }
  } return total
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


//Soal No. 4 (Array Multidimensi)
console.log('\nSoal No. 4 (Array Multidimensi)')
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
function dataHandling(input) {
  for( var i=0; i<input.length; i++) {
    console.log('Nomor ID:: ' + input [i][0])
    console.log('Nama Lengkap: ' + input [i][1])
    console.log('TTL: ' + input [i][2] + ' ' + input [i][3])
    console.log('Hobi: '  + input [i][4])
    console.log('\n')
  }
}
dataHandling(input)


//Soal No. 5 (Balik Kata)
console.log('Soal No. 5 (Balik Kata)')
function balikKata(string) {
  var KataAwal=string
  var KataBaru=''
  for (i=string.length-1; i>=0; i--) {
    KataBaru = KataBaru + KataAwal[i]
  } return KataBaru
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I


//Soal No. 6 (Metode Array)
console.log('\nSoal No. 6 (Metode Array)')
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input) {
  var data = input
  data.splice(1,2)
  data.splice(4,1)
  data.splice(1,0,"Roman Alamsyah Elsharawy")
  data.splice(2,0,"Provinsi Bandar Lampung")
  data.splice(4,0,"Pria")
  data.splice(5,0,"SMA Internasional Metro")
  console.log(data)

  var tanggal = data[3]
  var tanggalpisah = tanggal.split('/')
  var bulan = parseInt(tanggalpisah[1])
  switch(bulan) {
    case 01:  { console.log(NamaBulan = 'Januari'); break; }
    case 02:  { console.log(NamaBulan = 'Februari'); break; }
    case 03:  { console.log(NamaBulan = 'Maret'); break; }
    case 04:  { console.log(NamaBulan = 'April'); break; }
    case 05:  { console.log(NamaBulan = 'Mei'); break; }
    case 06:  { console.log(NamaBulan = 'Juni'); break; }
    case 07:  { console.log(NamaBulan = 'Juli'); break; }
    case 08:  { console.log(NamaBulan = 'Agustus'); break; }
    case 09:  { console.log(NamaBulan = 'September'); break; }
    case 10:  { console.log(NamaBulan = 'Oktober'); break; }
    case 11:  { console.log(NamaBulan = 'November'); break; }
    case 12:  { console.log(NamaBulan = 'Desember'); break; }
    default:  { console.log("Salah Nama Bulan"); break; }

  }
  var tanggalsort=tanggal.split('/')
  tanggalsort.sort(function (value1, value2) { return value2 - value1 } )
  console.log(tanggalsort)

  var tanggaljoin = tanggalpisah.join("-")
  console.log(tanggaljoin)

  var nama = data[1]
  var namapendek = nama.slice(0,15) 
  console.log(namapendek)
}
dataHandling2(input)
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 